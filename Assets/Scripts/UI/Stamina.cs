﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Stamina : MonoBehaviour
{

    public Slider staminaBarSlider;

    private int maxStamina = 100;

    public float currentStamina;

    private WaitForSeconds regenRate = new WaitForSeconds(0.1f);

    private Coroutine regen;


    // Start is called before the first frame update

    void Start()
    {
        currentStamina = maxStamina;
        staminaBarSlider.maxValue = maxStamina;
        staminaBarSlider.value = maxStamina;
    }



    public void UseStamina(float amount)
    {
        if (currentStamina - amount >= 0)
        {
            currentStamina -= amount;

            staminaBarSlider.value = currentStamina;

            if (regen != null)
            {
                StopCoroutine(regen);
            }

            regen = StartCoroutine(RegenStamina());
        }

    }



    private IEnumerator RegenStamina()
    {
        yield return new WaitForSeconds(4);
        while (currentStamina < maxStamina)
        {
            currentStamina += maxStamina / 100;

            staminaBarSlider.value = currentStamina;

            yield return regenRate;

        }

    }

}
