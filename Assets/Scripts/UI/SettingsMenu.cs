﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{

    public void SetSensitivity(float value)
    {
        print(value);
        DefaultSettings.sensitivity = value;
    }



    public void SetQuality(int quality)
    {
        QualitySettings.SetQualityLevel(quality);
        print(quality);
    }
}
