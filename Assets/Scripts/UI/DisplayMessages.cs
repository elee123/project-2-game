﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMessages : MonoBehaviour
{
    [SerializeField] private Text pressE;

    private void Start()
    {
        pressE.text = " ";
    }

    public void DisplayMessage()
    {
        pressE.text = "Press E";
    }

    public void DeleteMessage()
    {
        pressE.text = " ";
    }
}
