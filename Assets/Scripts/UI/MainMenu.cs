﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        FindObjectOfType<AudioManager>().Stop("Main Menu Music");
        //FindObjectOfType<AudioManager>().Play("Ambience");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void QuitGame()
    {
        print("Quit called");
        Application.Quit();

    }


    private void Start()
    {
        FindObjectOfType<AudioManager>().Play("Main Menu Music");
    }
}
