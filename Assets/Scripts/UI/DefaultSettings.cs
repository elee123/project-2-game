﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefaultSettings : MonoBehaviour
{
    //we need this static as menu will be unloaded when game starts
    //and we want to change a value in main scene.
    static public float sensitivity = 300;
    [SerializeField]
    private TMPro.TMP_Dropdown quality;
    [SerializeField]
    private Slider sensitivity_bar;

    //Do instantly on execution
    void Awake()
    {
        quality.value = 2;
        sensitivity_bar.value = sensitivity;
    }




}

