﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    // typical player states
    public bool isDead = false;
    public int health = 100;

    // related variables for rotating the player when it dies
    [SerializeField]
    private GameObject player;
    Quaternion deathRotation = Quaternion.Euler(90, 0, 0);
    private float rotateSpeed = 50f;

    // grab reference to this script which controls fading in/out and scene switching
    [SerializeField]
    FadeLevelChange fadeIn;

    // audio
    private AudioSource deathAudio;
    private bool isAudioPlayed = false;

    //damage indicator
    [SerializeField]
    private Light lighting;
    private float flashTimer = 0.5f;

    // caching
    private void Awake()
    {
        fadeIn = GameObject.FindWithTag("Fade").GetComponent<FadeLevelChange>();
        deathAudio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (health <= 0 )
        {
            isDead = true;
            if (!isAudioPlayed)
            {
                deathAudio.Play();
                isAudioPlayed = true;
            }
            //start doing death animation which is just a rotation
            player.transform.rotation = Quaternion.RotateTowards(player.transform.rotation, deathRotation, Time.deltaTime * rotateSpeed);

            //so we can interact with game over UI since the cursor is locked ingame (due to it being a first person game)
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            //fade in and switch scene to +1 build index (which is game over, file -> build settings to check the scene indexes)
            fadeIn.FadeToLevel(1);
        }

        if (lighting.color == Color.red)
        {
            flashTimer -= Time.deltaTime;

            if (flashTimer <= 0)
            {
                lighting.color = Color.black;
                flashTimer = 0.5f;
            }
        }
    }


    public void Damage(int amount)
    {
        if (health > 0)
        {
            FindObjectOfType<AudioManager>().Play("hurt");
        }
        lighting.color = Color.red;
        health -= amount;
    }



}
