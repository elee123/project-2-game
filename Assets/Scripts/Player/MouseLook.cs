﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MouseLook : MonoBehaviour
{
    [SerializeField] private PlayerHealth playerHealth;
    [SerializeField] float mouseSens;
    //Refer to the first person player object
    [SerializeField] Transform playerBody;

    float xRotation; 
    // Start is called before the first frame update
    void Start()
    {
        if (mouseSens != DefaultSettings.sensitivity)
        {
            mouseSens = DefaultSettings.sensitivity;
        }
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerHealth.isDead)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

            xRotation -= mouseY;

            //Make sure we dont over rotate around the player.
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            //Rotate the player's y axis based on mouseX
            playerBody.Rotate(Vector3.up * mouseX);
        }


    }
}
