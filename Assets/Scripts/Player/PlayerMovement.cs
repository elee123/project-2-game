﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    //if we're dead then no point doing this script
    [SerializeField]
    private PlayerHealth playerHealth;

    //character values 
    private const float walkSpeed = 5f;
    [SerializeField]
    CharacterController controller;
    public float speed;
    [SerializeField] float gravity = -9.81f;
    private Vector3 velocity;

    //jump
    [SerializeField] 
    float jumpHeight = 3f;
    [SerializeField] 
    const int jumpStaminaUsage = 5;

    //ground check
    [SerializeField] Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;

    //crouching
    //uncrouch only if it isnt hitting the ceiling
    [SerializeField]
    private Camera playerCamera;
    private bool crouched = false;
    private float originalHeight;
    private const float crouchHeight = 0.5f;
    [SerializeField]
    private const float lerpCrouchSpeed = 5f;
    private const float crouchSpeed = 2.5f;


    //running
    [SerializeField]
    private Stamina staminaBar;
    private const int runStaminaUsage = 10;
    private const float runSpeed = 10f;
    bool isSprinting = false;

    //audio
    [SerializeField]
    private AudioClip[] walkAudio;
    private AudioSource randomStep;
    private int moveMinimum = 1;
    private float footstepDelay = 0.5f;
    [SerializeField]
    private float audioTimer = 0f;



    private void Awake()
    {
        //Grab the height so we can adjust it for crouching
        originalHeight = controller.height;
        randomStep = gameObject.AddComponent<AudioSource>();
    }


    private void Movement()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        //make sure we dont get increased speed moving diagonally.
        if (move.magnitude > 1)
        {
            move /= move.magnitude;
        }

        controller.Move(move * speed * Time.deltaTime);

    }

    //Crouch down and stand up smoothly
    private void HandleCrouchSmoothness()
    {
        if (crouched)
        {
            controller.height = Mathf.Lerp(controller.height, crouchHeight, lerpCrouchSpeed * Time.deltaTime);
        }
        else if (!crouched )
        {
            controller.height = Mathf.Lerp(controller.height, originalHeight, lerpCrouchSpeed * Time.deltaTime);
        }
    }

    private void Grounded()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (controller.isGrounded && velocity.y < 0)
        {
            //not 0 since we need a little extra less to make sure we're grounded.
            velocity.y = -2f;
        }
    }
    // Physics based time step loop.
    void FixedUpdate()
    {
        if (!playerHealth.isDead)
        {
            Movement();
            HandleStepAudio();
            Jump();
            Crouch();
            Sprint();
            HandleCrouchSmoothness();
            QuitGame();

        }

        //check if on ground
        Grounded();

        //Simulate gravity
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    private void Update()
    {
        if (audioTimer >= 0)
        {
            audioTimer -= Time.deltaTime;
        }

        Debug.Log(isGrounded);
    }


    private void HandleStepAudio()
    {
        //if we're grounded and the walk velocity is at good pace, play step audio.
        if (controller.velocity.magnitude > moveMinimum && isGrounded && !isSprinting)
        {
            if (!randomStep.isPlaying && audioTimer <= 0)
            {
                randomStep.clip = walkAudio[Random.Range(0, walkAudio.Length)];
                randomStep.Play();
                audioTimer = footstepDelay;
            }

        }
        //if we're grounded and the walk velocity is at good pace, play step audio.
        else if (controller.velocity.magnitude > moveMinimum && isGrounded && isSprinting )
        {
            if (!randomStep.isPlaying && audioTimer <= 0)
            {
                randomStep.clip = walkAudio[Random.Range(0, walkAudio.Length)];
                randomStep.Play();
                audioTimer = 0.2f;
            }

        }
    }

    void Sprint()
    {
        //sprint, since its based off delta the value wont exactly be 0 when finished... hence the 0.1
        if (Input.GetKey(KeyCode.LeftShift) && (staminaBar.currentStamina > 0.1))
        {
            if (!crouched)
            {
                staminaBar.UseStamina(runStaminaUsage * Time.deltaTime);
                speed = runSpeed;
                isSprinting = true;
            }




        }
        else if (!crouched)
        {
            speed = walkSpeed;
            isSprinting = false;
        }

    }

    void Crouch()
    {
        if (Input.GetButtonDown("Crouch") && !crouched && !isSprinting)
        {
            crouched = true;
        }
        else if (Input.GetButtonDown("Crouch") && crouched && !isSprinting)
        {
            if (!Physics.Raycast(playerCamera.transform.position, Vector3.up, 1f))
            {
                Debug.Log("Nothing above you! uncrouching!");
                crouched = false;
            }
            else
            {
                Debug.Log("Something above you, cant uncrouch");
            }
        }

        if (crouched)
        {
            speed = crouchSpeed;
        }
        else if (!crouched)
        {
            speed = walkSpeed;
        }



    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded && staminaBar.currentStamina > jumpStaminaUsage)
        {
            staminaBar.UseStamina(jumpStaminaUsage);
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

    void QuitGame()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

    }
}
