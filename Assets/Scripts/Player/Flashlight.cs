﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    [SerializeField]
    private Light pointLight;
    private bool state = true;

    // Update is called once per frame
    void Update()
    {
        FlashlightSwitch();
    }


    void FlashlightSwitch()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            FindObjectOfType<AudioManager>().Play("Flashlight");
            if (state == false)
            {
                state = true;
                pointLight.enabled = true;
            }
            else
            {
                state = false;
                pointLight.enabled = false;
            }

        }

    }
}
