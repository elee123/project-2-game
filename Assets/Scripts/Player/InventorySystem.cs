﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour
{
    [SerializeField] private const int MAX_NUM_TRAPS = 5;
    private const int MIN_NUM_TRAPS = 0;
    private const int ADD_ONE = 1;

    //we start with zero traps
    private int current_num_traps = 0;

    public void AddItem()
    {
        current_num_traps += ADD_ONE;
        print("trap added to inventory");
    }

    public void RemoveItem()
    { 
        current_num_traps -= ADD_ONE;
        print("trap removed from the inventory");
    }

    public void CheckCurrentTrapCount()
    {
        print("current items in the inventory: " + GetCurrentTrapCount() + "/" + MAX_NUM_TRAPS);
    }

    public int GetCurrentTrapCount()
    {
        return current_num_traps;
    }

    public int GetMaxNumOfTraps()
    {
        return MAX_NUM_TRAPS;
    }

    public int GetMinNumOfTraps()
    {
        return MIN_NUM_TRAPS;
    }

    //display current number of traps
    public void OnGUI()
    {
        GUI.Label(new Rect(Screen.width - 70, Screen.height - 25, 140, 100), (GetCurrentTrapCount() + "/" + MAX_NUM_TRAPS).ToString());
    }

}
