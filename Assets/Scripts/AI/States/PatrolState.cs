﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolState : States
{


    //list of walkpoints for enemy to go to
    [SerializeField]
    private List<Transform> walkPointList = new List<Transform>();

    //AI
    [SerializeField]
    private NavMeshAgent navMeshAgent;

    private int currentDestination;

    //Patrol animation
    [SerializeField]
    Animator myAnimator;
    //return chase state player when detected
    [SerializeField]
    private ChaseState chaseState;

    //Player detection
    [SerializeField]
    GameObject player;
    [SerializeField]
    private LayerMask playerMask;
    [SerializeField]
    private LayerMask walls;
    [SerializeField]
    private LayerMask floor;
    public float sightRange;
    public bool canSeePlayer;

    //Audio
    private AudioSource walkAudio;

    private void Awake()
    {
        walkAudio = GetComponent<AudioSource>();
    }
    void Start()
    {
        //add a better randomness by using time
        Random.InitState(System.DateTime.Now.Millisecond);
        //set starter destination
        currentDestination = Random.Range(0, walkPointList.Count);
    }


    private void PatrolUpdate()
    {
        //go to destination set in Start()
        navMeshAgent.destination = walkPointList[currentDestination].position;

        //play walk animation 
        myAnimator.SetBool("Patrolling", true);

        //play walk audio
        if (!walkAudio.isPlaying)
        {
            walkAudio.Play();
        }

        //Move enemy
        Arrived();

        //View detection
        canSeePlayer = Physics.CheckSphere(transform.root.position, sightRange, playerMask) &&
                       !Physics.Raycast(transform.root.position, (player.transform.position - transform.root.position),sightRange, walls) &&
                       !Physics.Raycast(transform.root.position, (player.transform.position - transform.root.position), sightRange, floor);

    }


    public override States RunCurrentState()
    {
        //handle AI movement, audio, animation, detection. Basically an average Update function only ran during this state.
        PatrolUpdate();

        if (canSeePlayer)
        {
            myAnimator.SetBool("Patrolling", false);
            walkAudio.Stop();
            FindObjectOfType<AudioManager>().Play("Spotted");
            return chaseState;
        }
        return this;
        
    }


    private void Arrived()
    {
        //did the enemy arrive at waypoint? if so set a new random patrol waypoint
        if (navMeshAgent.remainingDistance <= 1)
        {
            currentDestination = Random.Range(0, walkPointList.Count);
            
        }

    }


}
