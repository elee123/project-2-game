﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCollisionDetect : MonoBehaviour
{
    [SerializeField]
    EnemyState enemyState;
    [SerializeField]
    TrapState trapState;
    [SerializeField]
    private AudioSource[] enemyAudioSource;
    private AudioSource trapAudio;
    private GameObject enemy;
    private Animator bearTrapAnim;
    private void Awake()
    {
        trapAudio = GetComponent<AudioSource>();
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        //we just want the audio the enemy outputs so we can stop it all when enemy gets trapped.
        //enemyAudioSource = enemy.GetComponentsInChildren<AudioSource>();
        enemyState = enemy.GetComponent<EnemyState>();
        trapState = enemy.GetComponentInChildren<TrapState>();
        bearTrapAnim = GetComponent<Animator>();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            enemyAudioSource = other.gameObject.transform.root.GetComponentsInChildren<AudioSource>();
            //stop all enemy sound when we enter a trap.
            foreach (AudioSource enemySound in enemyAudioSource)
            {
                enemySound.Stop();
            }
            bearTrapAnim.Play("BearTrapClose");
            trapAudio.Play();
            //switch to trap state, a bruteforce switch from the enemy state controller.
            other.gameObject.transform.root.GetComponent<EnemyState>().currentState = other.gameObject.transform.root.GetComponentInChildren<TrapState>();

            //destroy trap when timer is finished
            Destroy(gameObject, other.gameObject.transform.root.GetComponentInChildren<TrapState>().trapTimer);

        }
    }
}
