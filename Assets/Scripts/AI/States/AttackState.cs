﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackState : States
{
    //AI
    [SerializeField]
    private NavMeshAgent navMeshAgent;

    //Player
    [SerializeField]
    private PlayerHealth playerHealth;
    [SerializeField]
    private Transform player;
    const int enemyDamage = 20;

    //attack values
    private float timeBetweenAttacks = 2f;
    private bool alreadyAttacked = false;

    //state switches
    [SerializeField]
    ChaseState chaseState;

    //audio
    AudioSource attackAudio;

    private void Awake()
    {
        attackAudio = GetComponent<AudioSource>();
    }

    void AttackPlayer()
    {
        if (!alreadyAttacked)
        {
            //stop moving the enemy so it can attack
            navMeshAgent.isStopped = true;
            navMeshAgent.transform.LookAt(player);
            //play attack audio
            if (!attackAudio.isPlaying)
            {
                attackAudio.Play();
            }
            playerHealth.Damage(enemyDamage);
            Debug.Log("Attacked");
            alreadyAttacked = true;

        }
    }
    public override States RunCurrentState()
    {
        AttackPlayer();
        if (alreadyAttacked)
        {
            timeBetweenAttacks -= Time.deltaTime;

        }

        if (timeBetweenAttacks <= 0)
        {
            ResetAttack();
            attackAudio.Stop();
            return chaseState;
        }
        else
        {
            return this;
        }

    }
    void ResetAttack()
    {
        navMeshAgent.isStopped = false;
        alreadyAttacked = false;
        timeBetweenAttacks = 2f;
    }


}
