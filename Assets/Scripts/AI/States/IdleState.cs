﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : States
{
    public PatrolState patrolState;
    public bool patrol = true;

    public override States RunCurrentState()
    {
        if (patrol)
        {
            return patrolState;
        }
        else
        {
            Debug.Log("Idle state");
            return this;
        }

    }
}
