﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrapState : States
{
    [SerializeField]
    private NavMeshAgent navMeshAgent;
    [SerializeField]
    ChaseState chaseState; 
    [SerializeField]
    public float trapTimer = 5f;
    [SerializeField]
    Animator animator;
    bool animSet = false;

    AudioSource trapAudio;

    //cache audio
    private void Awake()
    {
        trapAudio = GetComponent<AudioSource>();
    }

    public override States RunCurrentState()
    {
        // play the animation and start the timer on how long the enemy is trapped for.
        animator.SetBool("Trapped", true);
        startTimer();
        // trap time over, we'll go back to chase state.
        // that state will determine if the player has ran off or is still in distance
        if (trapTimer <= 0)
        {
            resetTimer();
            return chaseState;
        }

        else
        {
            return this;
        }
    }

    // plays the trap related audio, force stop all animation, stop the enemy's movement (and velocity to enforce it)
    // and finally start the timer of it being trapped for.
    void startTimer()
    {
        if (trapTimer != 0f )
        {
            playTrapAudio();

            if (!animSet)
            {
                disableAllAnimation();
                navMeshAgent.velocity = Vector3.zero;
                navMeshAgent.isStopped = true;
                animSet = true;
            }
            trapTimer -= Time.deltaTime;
        }
    }

    //reset everything in startTimer
    void resetTimer()
    {
        if (animSet)
        {
            navMeshAgent.velocity = Vector3.one;
            navMeshAgent.isStopped = false;
            animator.SetBool("Trapped", false);
            animSet = false;
            //reset timer
            trapTimer = 5f;
        }
    }

    //enemy could be patrolling or chasing.. we'll just disable all animations to prevent annoying checks
    void disableAllAnimation()
    {
        //check all the bool params in animation and turn it all off
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.type == AnimatorControllerParameterType.Bool)
            {
                animator.SetBool(param.name, false);
            }
        }
            
    }

    //play only if it isn't already playing
    void playTrapAudio()
    {
        if (!trapAudio.isPlaying)
        {
            trapAudio.Play();
        }
    }
}
