﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyState : MonoBehaviour
{
    public States currentState;


    private void Update()
    {
        RunStateMachine();
    }
    private void RunStateMachine()
    {
        //? = if nextState is not null, execute runCurrentState()
        States nextState = currentState?.RunCurrentState();

        if (nextState != null)
        {
            SwitchState(nextState);
        }
    }



    private void SwitchState(States nextState)
    {
        currentState = nextState;

    }
}
