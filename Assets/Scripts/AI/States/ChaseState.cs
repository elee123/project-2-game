﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ChaseState : States
{

    //AI
    [SerializeField]
    NavMeshAgent navMeshAgent;

    //Chase values
    [SerializeField]
    private int enemyRunSpeed = 6;

    public bool isInAttackRange;
    public float attackRange;
    public float aggroDistance = 20f;

    // states we can switch to from this current state
    public AttackState attackState;
    [SerializeField]
    PatrolState patrolState;

    // both for distance checking each other to do various stuff such as attacking or patrolling
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Transform enemyPos;

    // animation
    [SerializeField]
    Animator animator;

    //audio
    AudioSource chaseAudio;

    private void Awake()
    {
        chaseAudio = GetComponent<AudioSource>();
        enemyPos = transform.root.GetComponent<Transform>();
    }

    private void ChaseUpdate()
    {
        startChasing();
        AttackRange();
    }



    public override States RunCurrentState()
    {
        ChaseUpdate();

        //if enemy is close to player... attack!
        if (isInAttackRange)
        {
            Debug.Log("In range to attack");
            disableChaseState();
            return attackState;
        }
        //if player has ran a distance away from the enemy... go patrol 
        if (distCheck(enemyPos.position, player.position) > aggroDistance)
        {
            Debug.Log("escaped!");
            disableChaseState();
            navMeshAgent.speed = 3.5f;
            return patrolState;
        }
        else
        {
            return this;
        }
    }

    public float distCheck(Vector3 a, Vector3 b)
    {

        return Vector3.Distance(a,b);


    }

    //disable current state animation and sound to prepare for next state
    private void disableChaseState()
    {
        animator.SetBool("Running", false);
        chaseAudio.Stop();
    }


    private void startChasing()
    {
        animator.SetBool("Running", true);
        navMeshAgent.SetDestination(player.position);
        navMeshAgent.speed = enemyRunSpeed;
        playRunAudio();

    }

    private void playRunAudio()
    {
        if (!chaseAudio.isPlaying)
        {
            chaseAudio.Play();
        }
    }

    private void AttackRange()
    {
        if (distCheck(enemyPos.position, player.position) <= attackRange)
        {
            isInAttackRange = true;
        }
        else
        {
            isInAttackRange = false;
        }
    }
}

