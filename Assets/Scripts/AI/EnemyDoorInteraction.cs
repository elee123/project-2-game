﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// since the game has doors, the enemy needs to go through the doors or it'll be stuck forever.
// we'll make a box collider and the door will open if enemy touches it (will be near the door)
public class EnemyDoorInteraction : MonoBehaviour
{
    [SerializeField]
    private DoorInteraction doorInteractionClass;
    [SerializeField]
    private DoorInteraction doorInteraction;

    private void Awake()
    {
        doorInteractionClass = transform.parent.gameObject.GetComponent<DoorInteraction>();
        doorInteraction = gameObject.transform.parent.GetComponent<DoorInteraction>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Enemy opening a door!");
            if (doorInteractionClass.isClosed && !doorInteractionClass.AnimatorPlaying())
            {
                doorInteractionClass.AnimateDoor();
            }
        }
    }
}
