﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameFinished : MonoBehaviour
{
    FadeLevelChange fader;
    private void Awake()
    {
        fader = GameObject.FindWithTag("Fade").GetComponent<FadeLevelChange>();
    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.L))
        {
            ObjectiveItemInteraction.pickedUp = 3;
        }
        if (ObjectiveItemInteraction.pickedUp >=3)
        {
            Debug.Log("game Finished");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            fader.FadeToLevel(2);
        }
    }
}
