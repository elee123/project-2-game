﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OutroScript : MonoBehaviour
{
    [SerializeField]
    private Image[] outroImages;
    private float timer = 2f;
    bool played = false;

    private void Awake()
    {
        FindObjectOfType<AudioManager>().Stop("Ambience");
    }
    //intro image order index is in order from start to finish: 0 -> 2
    private void Update()
    {
        if (outroImages[0].gameObject.activeSelf)
        {
            if (!played)
            {
                FindObjectOfType<AudioManager>().Play("runningOutro");
                played = true;
            }
            if (StartTimer(4))
            {
                outroImages[0].gameObject.SetActive(false);
                outroImages[1].gameObject.SetActive(true);
                played = false;

            }



        }

        if (outroImages[1].gameObject.activeSelf)
        {
            if (!played)
            {
                FindObjectOfType<AudioManager>().Play("roar");
                played = true;
            }
            if (StartTimer(5))
            {
                outroImages[1].gameObject.SetActive(false);
                outroImages[2].gameObject.SetActive(true);
                played = false;
            }


        }

        if (outroImages[2].gameObject.activeSelf)
        {
            if (!played)
            {
                FindObjectOfType<AudioManager>().Play("outroend");
                played = true;
            }
            if (StartTimer(2))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 4);
            }

        }



    }

    bool StartTimer(int duration)
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            ResetTimer(duration);
            return true;
        }
        return false;

    }

    void ResetTimer(int duration)
    {
        timer = duration;
    }

}
