﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeLevelChange : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    private int levelToLoad;

    public void FadeToLevel(int levelIdx)
    {
        levelToLoad = levelIdx;
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeComplete()
    {
        ObjectiveItemInteraction.pickedUp = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + levelToLoad);
    }
}

