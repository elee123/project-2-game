﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroScript : MonoBehaviour
{
    [SerializeField]
    private Image[] introImages;
    private float timer = 2f;
    bool played = false;

    private void Awake()
    {
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    //intro image order index is in order from start to finish: 0 -> 2
    private void Update()
    {
        if (introImages[0].gameObject.activeSelf)
        {
            if (!played)
            {
                FindObjectOfType<AudioManager>().Play("button");
                played = true;
            }
            if (StartTimer(4))
            {
                introImages[0].gameObject.SetActive(false);
                introImages[1].gameObject.SetActive(true);
                played = false;

            }



        }

        if (introImages[1].gameObject.activeSelf)
        {
            if (!played)
            {
                FindObjectOfType<AudioManager>().Play("electrocute");
                played = true;
            }
            if (StartTimer(2))
            {
                introImages[1].gameObject.SetActive(false);
                introImages[2].gameObject.SetActive(true);
                played = false;
            }


        }

        if (introImages[2].gameObject.activeSelf)
        {
            if (!played)
            {
                FindObjectOfType<AudioManager>().Play("heartbeat");
                played = true;
            }
            if (StartTimer(2))
            {
                FindObjectOfType<AudioManager>().Stop("heartbeat");
                FindObjectOfType<AudioManager>().Play("Ambience");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }

        }



    }

    bool StartTimer(int duration)
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            ResetTimer(duration);
            return true;
        }
        return false;

    }

    void ResetTimer(int duration)
    {
        timer = duration;
    }

}
