﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class traileronlyscript : MonoBehaviour
{
    [SerializeField]
    Animator monsterPrefab;
    Quaternion wantedRot = Quaternion.Euler(0, -87, 70);
    Quaternion origrot;
    float rotspeed = 100f;
    bool ready = false;

    private void Awake()
    {
        origrot = monsterPrefab.gameObject.transform.rotation;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Entered");
            monsterPrefab.Play("Peek");
            ready = true;
            monsterPrefab.gameObject.SetActive(true);
            FindObjectOfType<AudioManager>().Play("soundqueue");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        ready = false;
    }


    private void Update()
    {
        if (ready)
        {
            monsterPrefab.gameObject.transform.rotation = Quaternion.RotateTowards(monsterPrefab.gameObject.transform.rotation, wantedRot, Time.deltaTime * rotspeed);
        }
        if (!ready)
        {
            monsterPrefab.gameObject.transform.rotation = Quaternion.RotateTowards(monsterPrefab.gameObject.transform.rotation, origrot, Time.deltaTime * rotspeed);
        }
    }
}
