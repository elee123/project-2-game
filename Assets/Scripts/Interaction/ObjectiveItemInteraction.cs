﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveItemInteraction : MonoBehaviour
{
    ///distance within which player can interact with an object
    [SerializeField]
    private float distance = 4;

    [SerializeField]
    DisplayMessages displayMessages;
    private GameObject player;
    private float distToObject;

    //gameObject will be deleted when we pick up the item, we need a static to ensure its lifeline
    public static int pickedUp = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    private void OnMouseOver()
    {
        if (GameObject.FindWithTag("Objective"))
        {
            //get the distance between player and current trap cursor is over
            distToObject = Vector3.Distance(player.transform.position, gameObject.transform.position);

            if (distance >= distToObject)
            {
                displayMessages.DisplayMessage();
                if (Input.GetKeyDown(KeyCode.E))
                {
                    print("Objective picked!");
                    pickedUp += 1;
                    FindObjectOfType<AudioManager>().Play("Item Pickup");
                    displayMessages.DeleteMessage();
                    Destroy(gameObject);
                }
            }


        }
    }

    private void OnMouseExit()
    {
        if (GameObject.FindWithTag("Objective"))
        {
            displayMessages.DeleteMessage();
        }
    }
}
