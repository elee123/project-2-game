﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrapInteraction : MonoBehaviour
{
    ///distance within which player can interact with an object
    [SerializeField] 
    private float distance = 4;

    [SerializeField] 
    InventorySystem inventorySystem;

    [SerializeField]
    DisplayMessages displayMessages;

    private float currentDistance_PlayerToTrap;
    private GameObject player;


    private void Start()
    {
        player = GameObject.FindWithTag("Player");

        
    }

    private void OnMouseOver()
    {
        if(GameObject.FindWithTag("Trap"))
        {
            if(inventorySystem.GetCurrentTrapCount() != inventorySystem.GetMaxNumOfTraps())
            {
                //get the distance between player and current trap cursor is over
                currentDistance_PlayerToTrap = Vector3.Distance(player.transform.position, gameObject.transform.position);
                if(distance >= currentDistance_PlayerToTrap)
                {
                    displayMessages.DisplayMessage();
                    if (Input.GetKeyUp(KeyCode.E) && inventorySystem.GetCurrentTrapCount() != inventorySystem.GetMaxNumOfTraps())
                    {
                        inventorySystem.AddItem();
                        Destroy(gameObject);
                        displayMessages.DeleteMessage();
                    }
                }
            }
        }
    }

    private void OnMouseExit()
    {
        if (GameObject.FindWithTag("Trap"))
        {
            displayMessages.DeleteMessage();
        }
    }
}


