﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundInteraction : MonoBehaviour
{
    public InventorySystem inventorySystem;

    public GameObject trapPrefab;
    private GameObject preview, player;
    [SerializeField]
    private LayerMask groundMask;
    private float distToGround;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");

        preview = GameObject.Find("Trap Preview");
        preview.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.R) && inventorySystem.GetCurrentTrapCount() != inventorySystem.GetMinNumOfTraps())
        {
            Preview();
            //print("preview");
            if (Input.GetMouseButtonDown(0))
            {
                PutTheTrapDown();
                preview.SetActive(false);
            }
        }
        else if (Input.GetKeyUp(KeyCode.R))
        {
            preview.SetActive(false);
        }
    }

    public void PutTheTrapDown()
    { 
        SpawnTrap();
        inventorySystem.RemoveItem();
    }

    private void Preview()
    {
        preview.SetActive(true);
        Ray downRay = new Ray(preview.transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(downRay, out hit,  groundMask))
        {
            Debug.Log("Placeable trap");
            distToGround = hit.distance;
        }
    }
    private void SpawnTrap()
    {
        Vector3 placeMeHere = preview.transform.position;

        placeMeHere.y -= distToGround; 

        GameObject newTrap = Instantiate(trapPrefab, placeMeHere, player.transform.rotation);
        newTrap.name = "newTrap (clone)";
        newTrap.tag = "Trap";
    }

}


