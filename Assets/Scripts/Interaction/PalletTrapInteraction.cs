﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalletTrapInteraction : MonoBehaviour
{
    ///distance within which player can interact with an object
    [SerializeField]
    private float distance = 4;

    [SerializeField]
    DisplayMessages displayMessages;

    public Animator palletAnimator;

    private bool isFlipped;
    private GameObject player;
    private float currentDistance_PlayerToPallet;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        isFlipped = false;
    }

    private void OnMouseOver()
    {
        if (GameObject.FindWithTag("Pallet"))
        {
            //get the distance between player and current trap cursor is over
            currentDistance_PlayerToPallet = Vector3.Distance(player.transform.position, gameObject.transform.position);

            if (distance >= currentDistance_PlayerToPallet && isFlipped == false)
            {
                displayMessages.DisplayMessage();
                if(Input.GetKeyDown(KeyCode.E))
                {
                    print("Pallet flipped!");
                    palletAnimator.Play("FlippedOver");
                    isFlipped = true;
                    Destroy(gameObject, 7.0f);  //destroy after small delay
                }
            }


        }
    }

    private void OnMouseExit()
    {
        if (GameObject.FindWithTag("Pallet"))
        {
            displayMessages.DeleteMessage();
        }
    }
}
