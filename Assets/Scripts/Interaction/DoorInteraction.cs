﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class DoorInteraction : MonoBehaviour
{
    // Needed to get the player transform so we can do stuff when the player is close enough to the door.
    [SerializeField]
    private GameObject player;

    // the UI message reference.
    private DisplayMessages interactMsg;

    // grab dist from player so we know when to show the interact UI
    private float doorToPlayerDist;

    // distance the player has to be to see the UI from door.
    private const float interactionDist = 4;
    [SerializeField]
    public Animator myDoorAnim;
    [SerializeField]
    // used to reduce AI being stuck at the door, carving will add a path avoiding the door.
    private NavMeshObstacle doorObstacleNav;

    //door state
    public bool isOpen = false;
    public bool isClosed = true;

    //door open/close audio, both same.
    private AudioSource doorAudio;

    // cache the references
    private void Awake()
    {
        interactMsg = GameObject.FindWithTag("InteractMessage").GetComponentInChildren<DisplayMessages>();
        doorObstacleNav = GetComponent<NavMeshObstacle>();
        player = GameObject.FindWithTag("Player");
        myDoorAnim = GetComponent<Animator>();
        doorAudio = GetComponent<AudioSource>();
    }

    // if cursor is pointed at door...
    private void OnMouseOver()
    {
        // if its pointing at a door tag and the distance is good enough, do the door related stuff when E is pressed.
        if (GameObject.FindWithTag("Door"))
        {
            //only display interact msg if player is within a certain distance to a door.
            doorToPlayerDist = Vector3.Distance(gameObject.transform.position, player.transform.position);

            if (doorToPlayerDist <= interactionDist)
            {
                interactMsg.DisplayMessage();

                if (Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("E PRESSED");
                    AnimateDoor();

                }
            }
        }
    }

    // once mouse is not at door, we'll remove the interact UI
    private void OnMouseExit()
    {
        if (GameObject.FindWithTag("Door"))
        {
            interactMsg.DeleteMessage();
        }
    }

    //used to prevent E spamming the door and the door and it animation skipping.
    //only door open/closes when the animation is finished
    public bool AnimatorPlaying()
    {
        return myDoorAnim.GetCurrentAnimatorStateInfo(0).length > myDoorAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    
    public void AnimateDoor()
    {
        //if door is closed, open door
        if (isClosed && !AnimatorPlaying())
        {
            myDoorAnim.Play("DoorOpen");
            doorAudio.Play();
            isClosed = false;
            doorObstacleNav.carving = true;
            isOpen = true;
        }
        else if (isOpen && !AnimatorPlaying())
        {
            myDoorAnim.Play("DoorClose");
            doorAudio.Play();
            isOpen = false;
            doorObstacleNav.carving = false;
            isClosed = true;

        }
    }
}
